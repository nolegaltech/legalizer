<?php

namespace App\Tests\Service;

use App\Entity\Cookie;
use App\Service\CookieService;
use App\Service\CookieService\InvalidCookieException;

use PHPUnit\Framework\TestCase;


class CookieTest extends TestCase
{
    public function testGetCompleteIsTrueWhenItHasAllFields()
    {
        $theCookie = $this->aSampleCookie();

        $this->assertTrue($theCookie->getComplete());
    }

    public function testGetCompleteThrowsExceptionWhenItHasNoName()
    {
        $this->expectException(InvalidCookieException::class);

        $theCookie = $this->aSampleCookie();
        $theCookie->setName('');

        $theCookie->getComplete();
    }

    public function testGetCompleteThrowsExceptionWhenItHasNoOrigin()
    {
        $this->expectException(InvalidCookieException::class);

        $theCookie = $this->aSampleCookie();
        $theCookie->setOrigin('');

        $theCookie->getComplete();
    }

    public function testGetCompleteIsFalseWhenItHasNoGoal()
    {
        $theCookie = $this->aSampleCookie();
        $theCookie->setGoal('');

        $this->assertFalse($theCookie->getComplete());
    }

    public function testGetCompleteIsFalseWhenItHasNoType()
    {
        $theCookie = $this->aSampleCookie();
        $theCookie->setType('');

        $this->assertFalse($theCookie->getComplete());
    }

    private function aSampleCookie()
    {
        $cookie = new Cookie();
        $cookie->setName('aCookieName');
        $cookie->setOrigin('a-domain.com');
        $cookie->setPublished(new \DateTime());
        $cookie->setGoal('aGoal');
        $cookie->setType('aType');
        $cookie->setExpiration(999999);
        $cookie->setExpirationTime('an expiration time');
        $cookie->setPath('a/path');
        $cookie->setHttpOnly(true);
        $cookie->setThirdParty(true);
        $cookie->setSecure(true);
        $cookie->setUsageUrl('the-usage-url.com');
        $cookie->setOptoutUrl('the-optout-url.com');
        return $cookie;
    }

}
