<?php

namespace App\Tests\Service;

use App\Entity\AnalyzedWeb;
use App\Entity\Cookie;

use PHPUnit\Framework\TestCase;


class AnalyzedWebTest extends TestCase
{
    public function testPercentCookiesIs100WhenNoCookies()
    {
        $theAnalysis = $this->aSampleAnalysis();

        $this->assertEquals(100, $theAnalysis->getPercents()->getCookies());
    }

    public function testPercentCookiesIs50WhenSingleIncompleteCookie()
    {
        $theAnalysis = $this->aSampleAnalysis();
        $theAnalysis->addCookie($this->anIncompleteCookie());

        $this->assertEquals(50, $theAnalysis->getPercents()->getCookies());
    }

    public function testPercentCookiesIs33WhenTwoIncompleteCookies()
    {
        $theAnalysis = $this->aSampleAnalysis();
        $theAnalysis->addCookie($this->anIncompleteCookie());
        $theAnalysis->addCookie($this->anIncompleteCookie());

        $this->assertEquals(33, $theAnalysis->getPercents()->getCookies());
    }

    public function testPercentCookiesIs50WhenTwoIncompleteCookiesAndOneComplete()
    {
        $theAnalysis = $this->aSampleAnalysis();
        $theAnalysis->addCookie($this->anIncompleteCookie());
        $theAnalysis->addCookie($this->anIncompleteCookie());
        $theAnalysis->addCookie($this->aCompleteCookie());

        $this->assertEquals(50, $theAnalysis->getPercents()->getCookies());
    }

    public function testPercentPrivacyIs100WhenAllData()
    {
        $theAnalysis = $this->aSampleAnalysis();

        $this->assertEquals(100, $theAnalysis->getPercents()->getPrivacy());
    }

    public function testPercentPrivacyIs85WhenAllDataExceptOne()
    {
        $theAnalysis = $this->aSampleAnalysis();
        $theAnalysis->setCompanyName('');

        $this->assertEquals(85, $theAnalysis->getPercents()->getPrivacy());
    }

    public function testPercentPrivacyIs71WhenAllDataExceptTwo()
    {
        $theAnalysis = $this->aSampleAnalysis();
        $theAnalysis->setCompanyName('');
        $theAnalysis->setCompanyWeb('');

        $this->assertEquals(71, $theAnalysis->getPercents()->getPrivacy());
    }

    public function testPercentPrivacyIs57WhenAllDataExceptThree()
    {
        $theAnalysis = $this->aSampleAnalysis();
        $theAnalysis->setCompanyName('');
        $theAnalysis->setCompanyWeb('');
        $theAnalysis->setCompanyNif('');

        $this->assertEquals(57, $theAnalysis->getPercents()->getPrivacy());

        $theAnalysis = $this->aSampleAnalysis();
        $theAnalysis->setCompanyName('');
        $theAnalysis->setCompanyAddress('');
        $theAnalysis->setCompanyEmail('');

        $this->assertEquals(57, $theAnalysis->getPercents()->getPrivacy());
    }

    public function testPercentPrivacyIs14WhenNoCompanyData()
    {
        $theAnalysis = $this->aSampleAnalysis();
        $theAnalysis->setCompanyName('');
        $theAnalysis->setCompanyWeb('');
        $theAnalysis->setCompanyNif('');
        $theAnalysis->setCompanyAddress('');
        $theAnalysis->setCompanyEmail('');
        $theAnalysis->setCompanyArcoMail('');
        $this->assertEquals(14, $theAnalysis->getPercents()->getPrivacy());
    }

    public function testPercentTermsIs100()
    {
        $theAnalysis = $this->aSampleAnalysis();

        $this->assertEquals(100, $theAnalysis->getPercents()->getTerms());
    }

    public function testPercentFormsIs100()
    {
        $theAnalysis = $this->aSampleAnalysis();

        $this->assertEquals(100, $theAnalysis->getPercents()->getForms());
    }

    public function testPercentTotalIsAverageOfPercents()
    {
        $theAnalysis = $this->aSampleAnalysis();
        $this->assertEquals(100, $theAnalysis->getPercents()->getTotal());  // 100 + 100 + 100 + 100 / 4

        $theAnalysis->addCookie($this->anIncompleteCookie());
        $this->assertEquals( 87, $theAnalysis->getPercents()->getTotal());  //  50 + 100 + 100 + 100 / 4

        $theAnalysis->addCookie($this->anIncompleteCookie());
        $this->assertEquals( 83, $theAnalysis->getPercents()->getTotal());  //  33 + 100 + 100 + 100 / 4

        $theAnalysis->addCookie($this->anIncompleteCookie());
        $this->assertEquals( 81, $theAnalysis->getPercents()->getTotal());  //  25 + 100 + 100 + 100 / 4

        $theAnalysis->addCookie($this->aCompleteCookie());
        $this->assertEquals( 85, $theAnalysis->getPercents()->getTotal());  //  40 + 100 + 100 + 100 / 4
    }

    private function aSampleAnalysis()
    {
        $analysis = new AnalyzedWeb();
        $analysis->setUrl('http://an-url.com');
        $analysis->setHash('a-hash');
        $analysis->setCompanyName('A Company Name');
        $analysis->setCompanyNif('A NIF');
        $analysis->setCompanyAddress('the sample address');
        $analysis->setCompanyWeb('http://an-url.com');
        $analysis->setCompanyEmail('an-email@company.com');
        $analysis->setCompanyArcoMail('an-email@company.com');
        return $analysis;
    }

    private function aCompleteCookie()
    {
        $cookie = new Cookie();
        $cookie->setName('aCookieName');
        $cookie->setOrigin('a-domain.com');
        $cookie->setPublished(new \DateTime());
        $cookie->setGoal('aGoal');
        $cookie->setType('aType');
        $cookie->setExpiration(999999);
        $cookie->setExpirationTime('an expiration time');
        $cookie->setPath('a/path');
        $cookie->setHttpOnly(true);
        $cookie->setThirdParty(true);
        $cookie->setSecure(true);
        $cookie->setUsageUrl('the-usage-url.com');
        $cookie->setOptoutUrl('the-optout-url.com');
        return $cookie;
    }

    private function anIncompleteCookie()
    {
        $cookie = $this->aCompleteCookie();
        $cookie->setGoal('');
        return $cookie;
    }

}
