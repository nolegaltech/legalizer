<?php

namespace App\Tests\Service;

use App\Entity\Cookie;
use App\Service\CookieService;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class CookieServiceTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp() : void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testGetAll()
    {
        $cookies = new CookieService($this->entityManager);

        $all = $cookies->all();

        $this->assertEquals(1, count($all));
        $this->assertEquals('_ga', $all[0]->getName());
    }

    public function testFetchFromUri()
    {
        $cookies = new CookieService($this->entityManager);

        $all = $cookies->fetchFromUri('https://www.nolegaltech.com');

        $this->assertEquals(1, count($all));
        $this->assertEquals('PHPSESSID', $all[0]->getName());
    }
}


