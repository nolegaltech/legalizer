<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191118154725 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE webs_cookies (web_id INT NOT NULL, cookie_id INT NOT NULL, INDEX IDX_B8876AFBFE18474D (web_id), INDEX IDX_B8876AFB943BD662 (cookie_id), PRIMARY KEY(web_id, cookie_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE webs_cookies ADD CONSTRAINT FK_B8876AFBFE18474D FOREIGN KEY (web_id) REFERENCES analyzed_web (id)');
        $this->addSql('ALTER TABLE webs_cookies ADD CONSTRAINT FK_B8876AFB943BD662 FOREIGN KEY (cookie_id) REFERENCES cookie (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE webs_cookies');
    }
}
