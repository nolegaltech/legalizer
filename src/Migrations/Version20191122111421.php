<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191122111421 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE analyzed_web CHANGE company_name company_name VARCHAR(255) DEFAULT NULL, CHANGE company_nif company_nif VARCHAR(25) DEFAULT NULL, CHANGE company_address company_address VARCHAR(255) DEFAULT NULL, CHANGE company_web company_web VARCHAR(255) DEFAULT NULL, CHANGE company_email company_email VARCHAR(255) DEFAULT NULL, CHANGE company_arco_mail company_arco_mail VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE analyzed_web CHANGE company_name company_name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE company_nif company_nif VARCHAR(25) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE company_address company_address VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE company_web company_web VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE company_email company_email VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE company_arco_mail company_arco_mail VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
