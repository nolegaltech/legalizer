<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191121123505 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE analyzed_web ADD company_name VARCHAR(255) NOT NULL, ADD company_nif VARCHAR(25) NOT NULL, ADD company_address VARCHAR(255) NOT NULL, ADD company_web VARCHAR(255) NOT NULL, ADD company_email VARCHAR(255) NOT NULL, ADD company_arco_mail VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE analyzed_web DROP company_name, DROP company_nif, DROP company_address, DROP company_web, DROP company_email, DROP company_arco_mail');
    }
}
