<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Cookie;

class CookieFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $cookie = new Cookie();
        $cookie->setName('_ga');
        $cookie->setOrigin('google.com');
        $cookie->setPublished(new \DateTime());
        $cookie->setType('analytics');
        $cookie->setExpiration(62985600);
        $cookie->setPath('/');
        $cookie->setHttpOnly(0);
        $cookie->setThirdParty(1);
        $cookie->setSecure(0);
        $cookie->setUsageUrl('https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage?hl=es-419');
        $cookie->setOptoutUrl('https://tools.google.com/dlpage/gaoptout');

        $manager->persist($cookie);
        $manager->flush();
    }
}
