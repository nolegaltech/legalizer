<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Contracts\Translation\TranslatorInterface;


class ExceptionController extends AbstractController
{

    public function show(FlattenException $exception, DebugLoggerInterface $logger, TranslatorInterface $translator)
    {
        return $this->render('error/error.html.twig', [
            'message' => $translator->trans($exception->getMessage())
        ]);
    }

}
