<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;

use App\Entity\Cookie;
use App\Entity\Web;
use App\Form\Type\WebType;
use App\Form\Type\CompanyType;
use App\Service\Legalizer;

class WebController extends AbstractController
{
    /**
     * @Route("/web", name="legalize_web")
     */
    public function index(Request $request, Legalizer $legalizer)
    {
        $web = new Web();
        $form = $this->createForm(WebType::class, $web);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $legalizer->doWeb($web->getUrl());
            return $this->redirectToRoute('web_analysis', ['hash' => $hash]);
        }

        return $this->render('web/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/web/{hash}", name="web_analysis")
     */
    public function analysis($hash, Request $request, Legalizer $legalizer)
    {
        $analysis = $legalizer->getAnalysis($hash);
        return $this->render('web/analysis.html.twig', [
            'hash' => $hash,
            'analysis' => $analysis
        ]);
    }

    /**
     * @Route("/web/{hash}/edit", name="company_edit")
     */
    public function companyEdit($hash, Request $request, Legalizer $legalizer)
    {
        $analysis = $legalizer->getAnalysis($hash);
        $form = $this->createForm(CompanyType::class, $analysis);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $legalizer->persist($analysis);

            $callback = $request->query->get('callback');
            if (!empty($callback)) {
                return $this->redirect($callback);
            }
        }

        return $this->render('web/company-edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
