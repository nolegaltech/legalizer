<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Cookie;

class LicenseController extends AbstractController
{
    /**
     * @Route("/license", name="legalize_license")
     */
    public function index()
    {
        return $this->render('license/index.html.twig', [ ]);
    }
}
