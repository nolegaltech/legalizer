<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Cookie;
use App\Form\Type\CookieType;
use App\Service\CookieService;

class CookieController extends AbstractController
{
    /**
     * @Route("/cookie/{id}", name="cookie_show")
     */
    public function index(Cookie $cookie)
    {
        return $this->render('cookie/index.html.twig', [
            'cookie' => $cookie,
        ]);
    }

    /**
     * @Route("/cookie/{id}/edit", name="cookie_edit")
     */
    public function edit(Cookie $cookie, Request $request, CookieService $cookies)
    {
        $form = $this->createForm(CookieType::class, $cookie);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cookies->persist($cookie);

            $callback = $request->query->get('callback');
            if (!empty($callback)) {
                return $this->redirect($callback);
            }
        }

        return $this->render('cookie/edit.html.twig', [
            'cookie' => $cookie,
            'form' => $form->createView()
        ]);
    }
}
