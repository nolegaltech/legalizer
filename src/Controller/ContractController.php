<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Cookie;

class ContractController extends AbstractController
{
    /**
     * @Route("/contract", name="legalize_contract")
     */
    public function index()
    {
        return $this->render('contract/index.html.twig', [ ]);
    }
}
