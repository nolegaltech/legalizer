<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


use App\Entity\AnalyzedWeb;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('companyName', TextType::class, [
            'label' => 'form.company.name',
            'required' => false,
            'empty_data' => '',
            'translation_domain' => 'messages'
        ]);
        $builder->add('companyNif', TextType::class, [
            'label' => 'form.company.nif',
            'required' => false,
            'empty_data' => '',
            'translation_domain' => 'messages'
        ]);
        $builder->add('companyAddress', TextType::class, [
            'label' => 'form.company.address',
            'required' => false,
            'empty_data' => '',
            'translation_domain' => 'messages'
        ]);
        $builder->add('companyWeb', TextType::class, [
            'label' => 'form.company.web',
            'required' => false,
            'empty_data' => '',
            'translation_domain' => 'messages'
        ]);
        $builder->add('companyEmail', TextType::class, [
            'label' => 'form.company.email',
            'required' => false,
            'empty_data' => '',
            'translation_domain' => 'messages'
        ]);
        $builder->add('companyArcoMail', TextType::class, [
            'label' => 'form.company.arcoMail',
            'required' => false,
            'empty_data' => '',
            'translation_domain' => 'messages'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AnalyzedWeb::class,
        ]);
    }

}
