<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


use App\Entity\Cookie;

class CookieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'form.cookie.name',
            'disabled' => true,
            'translation_domain' => 'messages'
        ]);
        $builder->add('origin', TextType::class, [
            'label' => 'form.cookie.origin',
            'disabled' => true,
            'translation_domain' => 'messages'
        ]);
        $builder->add('expirationTime', TextType::class, [
            'label' => 'form.cookie.expirationTime',
            'disabled' => true,
            'translation_domain' => 'messages'
        ]);
        $builder->add('goal', TextType::class, [
            'label' => 'form.cookie.goal',
            'required' => false,
            'empty_data' => '',
            'translation_domain' => 'messages'
        ]);
        $builder->add('type', ChoiceType::class, [
            'label' => 'form.cookie.type',
            'choices'  => [
                '' => '',
                'Técnica' => 'Técnica',
                'Analítica' => 'Analítica',
                'Personalización' => 'Personalización'
            ],
            'required' => false,
            'empty_data' => '',
            'translation_domain' => 'messages'
        ]);
        $builder->add('usageUrl', TextType::class, [
            'label' => 'form.cookie.usageUrl',
            'required' => false,
            'empty_data' => '',
            'translation_domain' => 'messages'
        ]);
        $builder->add('optoutUrl', TextType::class, [
            'label' => 'form.cookie.optoutUrl',
            'required' => false,
            'empty_data' => '',
            'translation_domain' => 'messages'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cookie::class,
        ]);
    }

}
