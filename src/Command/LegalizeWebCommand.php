<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use App\Service\Legalizer;


class LegalizeWebCommand extends Command
{
    protected static $defaultName = 'legalize:web';

    private $legalizer;

    public function __construct(Legalizer $legalizer)
    {
        $this->legalizer = $legalizer;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates legal texts for a web.')
            ->setHelp('Creates legal texts for a web.')
            ->addArgument('url', InputArgument::REQUIRED, 'Url')
        ;

    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->legalizer->doWeb($input->getArgument('url'));
    }
}

