<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnalyzedWebRepository")
 */
class AnalyzedWeb
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\ManyToMany(targetEntity="Cookie",cascade={"persist"})
     * @ORM\JoinTable(name="webs_cookies",
     *      joinColumns={@ORM\JoinColumn(name="web_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="cookie_id", referencedColumnName="id")}
     * )
     */
    private $cookies;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hash;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyName;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $companyNif;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyWeb;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyArcoMail;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

    public function getCookies()
    {
        return $this->cookies;
    }

    public function addCookie(?Cookie $cookie): self
    {
        $this->cookies[] = $cookie;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getCompanyNif(): ?string
    {
        return $this->companyNif;
    }

    public function setCompanyNif(string $companyNif): self
    {
        $this->companyNif = $companyNif;

        return $this;
    }

    public function getCompanyAddress(): ?string
    {
        return $this->companyAddress;
    }

    public function setCompanyAddress(string $companyAddress): self
    {
        $this->companyAddress = $companyAddress;

        return $this;
    }

    public function getCompanyWeb(): ?string
    {
        return $this->companyWeb;
    }

    public function setCompanyWeb(string $companyWeb): self
    {
        $this->companyWeb = $companyWeb;

        return $this;
    }

    public function getCompanyEmail(): ?string
    {
        return $this->companyEmail;
    }

    public function setCompanyEmail(string $companyEmail): self
    {
        $this->companyEmail = $companyEmail;

        return $this;
    }

    public function getCompanyArcoMail(): ?string
    {
        return $this->companyArcoMail;
    }

    public function setCompanyArcoMail(string $companyArcoMail): self
    {
        $this->companyArcoMail = $companyArcoMail;

        return $this;
    }


    public function __construct()
    {
        $this->cookies = new ArrayCollection();
    }

    public function getPercents()
    {
        $percents = new Percentages();
        $percents->setCookies($this->getCookiesPercent());
        $percents->setPrivacy($this->getPrivacyPercent());
        $percents->setTerms($this->getTermsPercent());
        $percents->setForms($this->getFormsPercent());
        return $percents;
    }

    private function getCookiesPercent()
    {
        $count = 1;
        $total = 1;
        foreach($this->cookies as $cookie) {
            if ($cookie->getComplete()) {
                $count++;
            }
            $total++;
        }
        return 100 * $count / $total;
    }

    private function getPrivacyPercent()
    {
        $count = 1;
        $total = 7;
        if (!empty($this->companyName)) {
            $count++;
        }
        if (!empty($this->companyNif)) {
            $count++;
        }
        if (!empty($this->companyAddress)) {
            $count++;
        }
        if (!empty($this->companyWeb)) {
            $count++;
        }
        if (!empty($this->companyEmail)) {
            $count++;
        }
        if (!empty($this->companyArcoMail)) {
            $count++;
        }

        return 100 * $count / $total;
    }

    private function getTermsPercent()
    {
        return 100;
    }

    private function getFormsPercent()
    {
        return 100;
    }

}
