<?php

namespace App\Entity;

class Percentages
{

    private $cookies;
    private $privacy;
    private $terms;
    private $forms;

    public function getCookies(): ?int
    {
        return $this->cookies;
    }

    public function setCookies(int $cookies): self
    {
        $this->cookies = $cookies;

        return $this;
    }

    public function getPrivacy(): ?int
    {
        return $this->privacy;
    }

    public function setPrivacy(int $privacy): self
    {
        $this->privacy = $privacy;

        return $this;
    }

    public function getTerms(): ?int
    {
        return $this->terms;
    }

    public function setTerms(int $terms): self
    {
        $this->terms = $terms;

        return $this;
    }

    public function getForms(): ?int
    {
        return $this->forms;
    }

    public function setForms(int $forms): self
    {
        $this->forms = $forms;

        return $this;
    }

    public function getTotal(): ?int
    {
        return (
            $this->cookies +
            $this->privacy +
            $this->terms +
            $this->forms
        ) / 4;
    }

}
