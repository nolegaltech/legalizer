<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Web
{
    /**
     * @Assert\NotBlank(message="web.url.not_blank")
     * @Assert\Url(message="web.url.not_url")
     */
    private $url;

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

}
