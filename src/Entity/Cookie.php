<?php

namespace App\Entity;

use App\Service\CookieService\InvalidCookieException;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CookieRepository")
 */
class Cookie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $origin;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $published;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $goal = '';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type = '';

    /**
     * @ORM\Column(type="integer")
     */
    private $expiration = -1;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $expirationTime;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $httpOnly;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $thirdParty;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $secure;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $usageUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $optoutUrl;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    public function setOrigin(string $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    public function getPublished(): ?\DateTimeInterface
    {
        return $this->published;
    }

    public function setPublished(\DateTimeInterface $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getGoal(): ?string
    {
        return $this->goal;
    }

    public function setGoal(string $goal): self
    {
        $this->goal = $goal;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getExpiration(): ?int
    {
        return $this->expiration;
    }

    public function setExpiration(int $expiration): self
    {
        $this->expiration = $expiration;

        return $this;
    }

    public function getExpirationTime(): ?string
    {
        return $this->expirationTime;
    }

    public function setExpirationTime(string $expirationTime): self
    {
        $this->expirationTime = $expirationTime;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getHttpOnly(): ?bool
    {
        return $this->httpOnly;
    }

    public function setHttpOnly(bool $httpOnly): self
    {
        $this->httpOnly = $httpOnly;

        return $this;
    }

    public function getThirdParty(): ?bool
    {
        return $this->thirdParty;
    }

    public function setThirdParty(bool $thirdParty): self
    {
        $this->thirdParty = $thirdParty;

        return $this;
    }

    public function getSecure(): ?bool
    {
        return $this->secure;
    }

    public function setSecure(bool $secure): self
    {
        $this->secure = $secure;

        return $this;
    }

    public function getUsageUrl(): ?string
    {
        return $this->usageUrl;
    }

    public function setUsageUrl(?string $usageUrl): self
    {
        $this->usageUrl = $usageUrl;

        return $this;
    }

    public function getOptoutUrl(): ?string
    {
        return $this->optoutUrl;
    }

    public function setOptoutUrl(?string $optoutUrl): self
    {
        $this->optoutUrl = $optoutUrl;

        return $this;
    }

    public function getComplete()
    {
        if (empty($this->name) || empty($this->origin)) {
            throw new InvalidCookieException();
        }
        return (!empty($this->goal) && !empty($this->type));
    }

    public function __toString()
    {
        return '    Cookie {'
             . '        id = '             . $this->getId()
             . '        name = '           . $this->getName()
             . '        origin = '         . $this->getOrigin()
             . '        published = '      . $this->getPublished()
             . '        goal = '           . $this->getGoal()
             . '        type = '           . $this->getType()
             . '        expiration = '     . $this->getExpiration()
             . '        expirationTime = ' . $this->getExpirationTime()
             . '        path = '           . $this->getPath()
             . '        httpOnly = '       . $this->getHttpOnly()
             . '        thirdParty = '     . $this->getThirdParty()
             . '        secure = '         . $this->getSecure()
             . '        usageUrl = '       . $this->getUsageUrl()
             . '        optoutUrl = '      . $this->getOptoutUrl()
             . '    }';
    }
}
