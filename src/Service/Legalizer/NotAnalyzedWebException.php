<?php

namespace App\Service\Legalizer;


class NotAnalyzedWebException extends \Exception implements LegalizerException
{

    public function __construct()
    {
        parent::__construct(
            "This web has not been analyzed",
            LegalizerException::NOT_ANALYZED_EXCEPTION
        );
    }

}
