<?php

namespace App\Service\Legalizer;


interface LegalizerException
{
    const ALREADY_ANALYZED_EXCEPTION = 1;
    const NOT_ANALYZED_EXCEPTION = 2;
}
