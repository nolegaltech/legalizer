<?php

namespace App\Service\Legalizer;


class AlreadyAnalyzedWebException extends \Exception implements LegalizerException
{

    public function __construct()
    {
        parent::__construct(
            "This web has already been analyzed",
            LegalizerException::ALREADY_ANALYZED_EXCEPTION
        );
    }

}
