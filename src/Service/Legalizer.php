<?php

namespace App\Service;

use App\Entity\AnalyzedWeb;
use App\Repository\AnalyzedWebRepository;
use App\Repository\CookieRepository;
use App\Service\CookieService;
use App\Service\HashGenerator;
use App\Service\Legalizer\AlreadyAnalyzedWebException;
use App\Service\Legalizer\NotAnalyzedWebException;

use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

use \Twig\Environment;

use Doctrine\ORM\EntityManagerInterface;


class Legalizer {

    /**
     * @var App\Service\CookieService
     */
    private $cookieService;

    /**
     * @var App\Service\HashGenerator
     */
    private $hashGenerator;

    /**
     * @var \Twig\Environment
     */
    private $twig;

    /**
     * @var App\Repository\AnalyzedWebRepository
     */
    private $websRepository;

    /**
     * @var App\Repository\CookieRepository
     */
    private $cookiesRepository;

    /**
     * @var Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    /**
     * @var Symfony\Component\Filesystem\Filesystem
     */
    private $files;

    public function __construct(CookieService $cookieService, Environment $twig, HashGenerator $hashGenerator, AnalyzedWebRepository $websRepository, CookieRepository $cookiesRepository, EntityManagerInterface $em, Filesystem $files)
    {
        $this->cookieService = $cookieService;
        $this->twig = $twig;
        $this->hashGenerator = $hashGenerator;
        $this->em = $em;
        $this->websRepository = $websRepository;
        $this->cookiesRepository = $cookiesRepository;
        $this->files = $files;
    }

    public function doWeb($url)
    {
        $found = $this->websRepository->findByUrl($url);
        if (count($found) > 0) {
            throw new AlreadyAnalyzedWebException();
        }

        $theHash = $this->hashGenerator->generate();

        $analyzed_web = new AnalyzedWeb();
        $analyzed_web->setUrl($url);
        $analyzed_web->setHash($theHash);

        $fetched_cookies = $this->cookieService->fetchFromUri($analyzed_web->getUrl());
        foreach ($fetched_cookies as $cookie) {
            $analyzed_web->addCookie($cookie);
            $found = $this->cookiesRepository->findBy([
                'name' => $cookie->getName(),
                'origin' => $cookie->getOrigin()
            ]);
            if (count($found) != 1) {
                $cookie->setPublished(new \DateTime());
                if ($cookie->getOrigin() == $url) {
                    $cookie->setThirdParty(1);
                } else {
                    $cookie->setThirdParty(0);
                }
                $this->em->persist($cookie);
                $this->em->flush();
            }
        }

        $this->em->persist($analyzed_web);
        $this->em->flush();

        return $theHash;
    }

    public function getAnalysis($hash)
    {
        $found = $this->websRepository->findByHash($hash);
        if (count($found) != 1) {
            throw new NotAnalyzedWebException();
        }

        $theAnalysis = $found[0];

        $this->updateFiles($theAnalysis);

        return $theAnalysis;
    }

    public function persist(AnalyzedWeb $analysis)
    {
        $this->em->persist($analysis);
        $this->em->flush();
    }

    private function updateFiles($analysis)
    {
        $analysisFolder = 'files/' . $analysis->getHash();
        $this->files->mkdir($analysisFolder);
        $this->files->dumpFile(
            $analysisFolder . '/cookies.md',
            $this->twig->render('texts/cookies.md.twig',
                [
                    'site' => $analysis->getUrl(),
                    'cookies' => $analysis->getCookies()
                ]
            )
        );
    }

}
