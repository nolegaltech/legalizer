<?php

namespace App\Service;

class HashGenerator {

    private $allowed_chars;
    private $no_special_chars;

    public function __construct() {
        $this->allowed_chars = array(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
            '.', '-', '_', '$', '!'
        );
        $this->no_special_chars = array(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'
        );
    }

    public function generate($length = 32, $with_special_chars = false) {
        $result = "";
        $possible_chars = ($with_special_chars)
            ? $this->allowed_chars
            : $this->no_special_chars;
        $last_index = count($possible_chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $result .= $possible_chars[rand(0, $last_index)];
        }
        return $result;
    }

}
