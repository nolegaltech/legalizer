<?php

namespace App\Service\CookieService;


interface CookieServiceException
{
    const TRIKY_NOT_AVAILABLE = 1;
    const INVALID_COOKIE = 2;
}
