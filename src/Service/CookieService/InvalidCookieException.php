<?php

namespace App\Service\CookieService;


class InvalidCookieException extends \Exception implements CookieServiceException
{

    public function __construct()
    {
        parent::__construct(
            "This cookie contains invalid data",
            CookieServiceException::INVALID_COOKIE
        );
    }

}
