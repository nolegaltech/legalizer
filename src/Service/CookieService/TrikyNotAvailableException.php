<?php

namespace App\Service\CookieService;


class TrikyNotAvailableException extends \Exception implements CookieServiceException
{

    public function __construct(\Exception $e)
    {
        parent::__construct(
            "Triky is not available",
            CookieServiceException::TRIKY_NOT_AVAILABLE,
            $e
        );
    }

}
