<?php

namespace App\Service;

use Doctrine\ORM\EntityManager;
use App\Entity\Cookie;
use App\Service\CookieService\TrikyNotAvailableException;

use Doctrine\ORM\EntityManagerInterface;

class CookieService {

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    public function all()
    {
        $cookieRepository = $this->em
            ->getRepository(Cookie::class);
        return $cookieRepository->findAll();
    }

    public function fetchFromUri($uri)
    {
        try {
            $response = json_decode(file_get_contents('https://triky-server.herokuapp.com/cookies/' . urlencode($uri)));
        } catch(\ErrorException $e) {
            throw new TrikyNotAvailableException($e);
        }
        $cookies = [];
        foreach ($response->cookies as $cookie) {
            $cookies []= $this->createCookieFromTriky($cookie);
        }
        return $cookies;
    }

    private function createCookieFromTriky($data)
    {
        $cookie = new Cookie();
        if (isset($data->name)) {
            $cookie->setName($data->name);
        }
        if (isset($data->domain)) {
            $cookie->setOrigin($data->domain);
        }
        if (isset($data->expiry)) {
            $cookie->setExpiration($data->expiry);
        }
        if (isset($data->path)) {
            $cookie->setPath($data->path);
        }
        if (isset($data->httponly)) {
            $cookie->setHttpOnly($data->httponly);
        }
        if (isset($data->secure)) {
            $cookie->setSecure($data->secure);
        }
        if (isset($data->expirationTime)) {
            $cookie->setExpirationTime($data->expirationTime);
        }
        return $cookie;
    }

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    public function persist(Cookie $cookie)
    {
        $this->em->persist($cookie);
        $this->em->flush();
    }

}
