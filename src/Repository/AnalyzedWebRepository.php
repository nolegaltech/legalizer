<?php

namespace App\Repository;

use App\Entity\AnalyzedWeb;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AnalyzedWeb|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnalyzedWeb|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnalyzedWeb[]    findAll()
 * @method AnalyzedWeb[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnalyzedWebRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnalyzedWeb::class);
    }

    // /**
    //  * @return AnalyzedWeb[] Returns an array of AnalyzedWeb objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AnalyzedWeb
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
